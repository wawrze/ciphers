package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms;

public abstract class Algorithm {

    protected final int NUMBER_OF_CHARACTERS = 95;
    protected final int MIN_CHAR_NUMBER = 32;
    protected final String key;

    public Algorithm(String key) {
        this.key = key;
    }

    public abstract String decrypt(String text);

    public abstract String encrypt(String text);

    protected String simplifyText(String text) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if (
                    text.charAt(i) >= MIN_CHAR_NUMBER
                            && text.charAt(i) < MIN_CHAR_NUMBER + NUMBER_OF_CHARACTERS
            ) {
                result.append(text.charAt(i));
            }
        }
        return result.toString();
    }

}