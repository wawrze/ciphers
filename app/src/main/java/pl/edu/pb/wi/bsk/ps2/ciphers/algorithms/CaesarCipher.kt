package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms

import pl.edu.pb.wi.bsk.ps2.ciphers.R
import pl.edu.pb.wi.bsk.ps2.ciphers.others.WrongKeyFormatException

class CaesarCipher(key: String) : Algorithm(key) {

    private var k: Int

    init {
        try {
            k = key.toInt()
            if (k < 1) throw WrongKeyFormatException(R.string.should_be_numeric_format)
        } catch (e: Throwable) {
            throw WrongKeyFormatException(R.string.should_be_numeric_format)
        }
    }

    override fun encrypt(text: String): String {
        val simplifiedText = simplifyText(text)
        val encrypted = StringBuilder()
        for (char: Char in simplifiedText.toCharArray()) {
            val a = char.toInt() - MIN_CHAR_NUMBER
            val c = (a + k) % NUMBER_OF_CHARACTERS
            encrypted.append((c + MIN_CHAR_NUMBER).toChar())
        }
        return encrypted.toString()
    }

    override fun decrypt(text: String): String {
        val simplifiedText = simplifyText(text)
        val decrypted = StringBuilder()
        for (char: Char in simplifiedText.toCharArray()) {
            val c = char.toInt() - MIN_CHAR_NUMBER
            val a = (c + (NUMBER_OF_CHARACTERS - k)) % NUMBER_OF_CHARACTERS
            decrypted.append((a + MIN_CHAR_NUMBER).toChar())
        }
        return decrypted.toString()
    }

}