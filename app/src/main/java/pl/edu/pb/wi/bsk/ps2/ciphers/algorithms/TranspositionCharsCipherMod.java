package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms;

import pl.edu.pb.wi.bsk.ps2.ciphers.R;
import pl.edu.pb.wi.bsk.ps2.ciphers.others.KeyTransform;
import pl.edu.pb.wi.bsk.ps2.ciphers.others.WrongKeyFormatException;

public class TranspositionCharsCipherMod extends Algorithm {

    private final char[] keyChars;

    public TranspositionCharsCipherMod(String key) throws WrongKeyFormatException {
        super(key);
        keyChars = key.toCharArray();
        try {
            for (char keyChar : keyChars) {
                if (keyChar < 64 || keyChar > 122) {
                    throw new WrongKeyFormatException(R.string.should_be_char_format);
                }
            }
        } catch (Exception e) {
            throw new WrongKeyFormatException(R.string.should_be_char_format);
        }
    }

    @Override
    public String decrypt(String text) {
        int[] permutationOrder = KeyTransform.charsKeyTransform(key, keyChars);
        int[] invertedPermutation = new int[permutationOrder.length];
        int col = permutationOrder.length;
        int row = rowMultiply(text) * col;

        char[][] matrix = new char[row][col];
        StringBuilder decryptedString = new StringBuilder();

        for (int i = 0; i < permutationOrder.length; i++) {
            invertedPermutation[permutationOrder[i] - 1] = i + 1;
        }
        for (int i = 0, k = 0, rowCounterReset = 0, crossingIndex = 0; i < row; i++) {
            for (int a = 0; a < permutationOrder.length; a++) {
                if (rowCounterReset == (permutationOrder[a] - 1)) crossingIndex = a;
            }
            for (int j = 0; j < col; j++) {
                if (k < text.length()) {
                    if (j <= crossingIndex) {
                        matrix[i][j] = 252;
                        k++;
                    } else {
                        matrix[i][j] = 251;
                    }
                } else {
                    matrix[i][j] = 251;
                }
            }
            if (((i + 1) % col == 0) && i != 0) {
                rowCounterReset = 0;
            } else {
                rowCounterReset++;
            }
        }
        for (int j = 0, k = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                if (k < text.length()) {
                    if ((matrix[i][invertedPermutation[j] - 1]) == 252) {
                        matrix[i][invertedPermutation[j] - 1] = text.charAt(k);
                        k++;
                    }
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (matrix[i][j] != 251) decryptedString.append(matrix[i][j]);
            }
        }
        return decryptedString.toString();
    }

    @Override
    public String encrypt(String text) {
        int[] permutationOrder = KeyTransform.charsKeyTransform(key, keyChars);
        int col = permutationOrder.length;
        int row = rowMultiply(text) * col;

        char[][] matrix = new char[row][col];
        char[][] encrypted = new char[row][col];
        StringBuilder encryptedString = new StringBuilder();

        for (int i = 0, k = 0, rowCounterReset = 0, crossingIndex = 0; i < row; i++) {
            for (int a = 0; a < permutationOrder.length; a++) {
                if (rowCounterReset == (permutationOrder[a] - 1)) crossingIndex = a;
            }
            for (int j = 0; j < col; j++) {
                if (k < text.length()) {
                    if (j <= crossingIndex) {
                        matrix[i][j] = text.charAt(k);
                        k++;
                    } else {
                        matrix[i][j] = 251;
                    }
                } else {
                    matrix[i][j] = 251;
                }
            }
            if (((i + 1) % col == 0) && i != 0) {
                rowCounterReset = 0;
            } else {
                rowCounterReset++;
            }
        }
        for (int j = 0; j < col; j++) {
            for (int i = 0; i < row; i++) encrypted[i][permutationOrder[j] - 1] = matrix[i][j];
        }
        for (int j = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                if (encrypted[i][j] != 251) encryptedString.append(encrypted[i][j]);
            }
        }
        return encryptedString.toString();
    }

    private double sumFrom1ToN(int n) {
        int result = 0;
        for (int x = 1; x <= n; x++) result += x;
        return result;
    }

    /**
     * calculates row count for short keys/long words combo
     */
    private int rowMultiply(String text) {
        double rowRoundVariable = Math.round(text.length() / sumFrom1ToN(key.length()));
        if (rowRoundVariable % 1 == 1) {
            return (int) rowRoundVariable;
        } else {
            return (int) rowRoundVariable + 1;
        }
    }

}