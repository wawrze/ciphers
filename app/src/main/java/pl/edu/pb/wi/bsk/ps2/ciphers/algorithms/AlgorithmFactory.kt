package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms

import pl.edu.pb.wi.bsk.ps2.ciphers.others.AlgorithmTypes

object AlgorithmFactory {
    fun getAlgorithm(type: AlgorithmTypes, key: String) = when (type) {
        AlgorithmTypes.RAIL_FENCE -> RailFenceCipher(key)
        AlgorithmTypes.TRANSPOSITION_CHARS -> TranspositionCharsCipher(key)
        AlgorithmTypes.TRANSPOSITION_NUMERIC -> TranspositionNumeric(key)
        AlgorithmTypes.CAESAR_CIPHER -> CaesarCipher(key)
        AlgorithmTypes.TRANSPOSITION_CHARS_MOD -> TranspositionCharsCipherMod(key)
        AlgorithmTypes.VIGENERE_CIPHER -> VigenereCipher(key)
    }
}