package pl.edu.pb.wi.bsk.ps2.ciphers.others;

public class KeyTransform {

    /**
     * transforms the char key into array with int values
     */
    public static int[] charsKeyTransform(String key, char[] keyChars) {
        int[] permutationOrder = new int[key.length()];
        int min = 127;
        int counter = 1;
        int temp = 0;

        for (int j = 0; j < key.length(); j++) {
            for (int i = 0; i < key.length(); i++) {
                if (keyChars[i] < min) {
                    min = keyChars[i];
                    temp = i;
                }
            }
            min = 127;
            permutationOrder[temp] = counter;
            keyChars[temp] = 128;
            counter++;
        }
        return permutationOrder;
    }

    public static int[] numericKeyTransform(String key) {
        String[] integerStrings = key.split("-");
        int[] permutationOrder = new int[integerStrings.length];

        for (int i = 0; i < permutationOrder.length; i++) {
            permutationOrder[i] = Integer.parseInt(integerStrings[i]);
        }

        return permutationOrder;
    }

}