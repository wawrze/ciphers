package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms;

import pl.edu.pb.wi.bsk.ps2.ciphers.R;
import pl.edu.pb.wi.bsk.ps2.ciphers.others.KeyTransform;
import pl.edu.pb.wi.bsk.ps2.ciphers.others.WrongKeyFormatException;

public class TranspositionCharsCipher extends Algorithm {

    private final char[] keyChars;

    public TranspositionCharsCipher(String key) throws WrongKeyFormatException {
        super(key);
        keyChars = key.toCharArray();
        try {
            for (char keyChar : keyChars) {
                if (keyChar < 64 || keyChar > 122) {
                    throw new WrongKeyFormatException(R.string.should_be_char_format);
                }
            }
        } catch (Exception e) {
            throw new WrongKeyFormatException(R.string.should_be_char_format);
        }
    }

    @Override
    public String decrypt(String text) {
        int[] permutationOrder = KeyTransform.charsKeyTransform(key, keyChars);
        int[] invertedPermutation = new int[permutationOrder.length];
        int col = permutationOrder.length;
        int row = rowsAmount(text, col);

        char[][] matrix = new char[row][col];
        StringBuilder decryptedString = new StringBuilder();
        for (int i = 0; i < permutationOrder.length; i++) {
            invertedPermutation[permutationOrder[i] - 1] = i + 1;
        }
        int arraySize = row * col;
        int charactersLeft = arraySize - text.length();
        for (int c = 0; c < charactersLeft; c++) {
            matrix[row - 1][col - 1 - c] = 251;
        }
        for (int j = 0, k = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                if (k < text.length()) {
                    if (matrix[i][invertedPermutation[j] - 1] != 251) {
                        matrix[i][invertedPermutation[j] - 1] = text.charAt(k);
                        k++;
                    }
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (matrix[i][j] != 251) decryptedString.append(matrix[i][j]);
            }
        }
        return decryptedString.toString();
    }

    @Override
    public String encrypt(String text) {
        int[] permutationOrder = KeyTransform.charsKeyTransform(key, keyChars);
        int col = permutationOrder.length;
        int row = rowsAmount(text, col);
        StringBuilder encryptedString = new StringBuilder();

        if (text.length() % col >= 1) row++;

        char[][] matrix = new char[row][col];
        char[][] encrypted = new char[row][col];

        for (int i = 0, k = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (k < text.length()) {
                    matrix[i][j] = text.charAt(k);
                    k++;
                } else {
                    matrix[i][j] = 251;
                }
            }
        }
        for (int j = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                encrypted[i][permutationOrder[j] - 1] = matrix[i][j];
            }
        }
        for (int j = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                if (encrypted[i][j] != 251) encryptedString.append(encrypted[i][j]);
            }
        }
        return encryptedString.toString();
    }

    private int rowsAmount(String text, int col) {
        if ((text.length() % col) == 0) {
            return text.length() / col;
        } else {
            return text.length() / col + 1;
        }
    }

}