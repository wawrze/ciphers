package pl.edu.pb.wi.bsk.ps2.ciphers.others

import android.content.ContentResolver
import android.net.Uri
import android.os.Environment
import android.util.Log
import java.io.File

class FileManager {

    companion object {
        private const val OUTPUT_FILE_NAME = "/output.txt"
    }

    fun writeToFile(text: String) = try {
        @Suppress("DEPRECATION") val outputPath = Environment.getExternalStorageDirectory()
            .absolutePath + OUTPUT_FILE_NAME
        val file = File(outputPath)
        file.delete()
        file.exists()
        file.appendText(text)
        true
    } catch (e: Throwable) {
        Log.e("FileManager", "Write to file error: ", e)
        false
    }

    fun readFromFile(uri: Uri, contentResolver: ContentResolver) = try {
        val inputStream = contentResolver.openInputStream(uri)
        val text = inputStream?.readBytes()?.toString(Charsets.UTF_8)
        inputStream?.close()
        text
    } catch (e: Throwable) {
        Log.e("FileManager", "Read from file error: ", e)
        ""
    }

}