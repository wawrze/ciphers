package pl.edu.pb.wi.bsk.ps2.ciphers.others

enum class AlgorithmTypes(val algorithmName: String, val keyType: KeyTypes) {
    RAIL_FENCE("Szyfr płotkowy", KeyTypes.NUMERIC),
    TRANSPOSITION_NUMERIC("Szyfr przestawieniowy z kluczem numerycznym", KeyTypes.CHAR),
    TRANSPOSITION_CHARS("Szyfr przestawieniowy z kluczem znakowym", KeyTypes.CHAR),
    TRANSPOSITION_CHARS_MOD("Ulepszony szyfr przestawieniowy z kluczem znakowym", KeyTypes.CHAR),
    CAESAR_CIPHER("Szyfr Cezara", KeyTypes.NUMERIC),
    VIGENERE_CIPHER("Szyfr Vigenere'a", KeyTypes.CHAR);

    companion object {
        fun getTypeForName(value: String) = values().firstOrNull {
            it.algorithmName == value
        } ?: RAIL_FENCE
    }
}