package pl.edu.pb.wi.bsk.ps2.ciphers.others

class WrongKeyFormatException(val messageRes: Int) : Exception()