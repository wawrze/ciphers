package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms

class VigenereCipher(key: String) : Algorithm(key) {

    private fun getKey(textLength: Int): String {
        val simplifiedKey = simplifyText(key).toCharArray()
        val result = StringBuilder()
        for (i in 0..textLength) {
            result.append(simplifiedKey[i % simplifiedKey.size])
        }
        return result.toString()
    }

    override fun encrypt(text: String): String {
        val simplifiedText = simplifyText(text).toCharArray()
        val generatedKey = getKey(simplifiedText.size).toCharArray()
        val encrypted = StringBuilder()
        for (i in simplifiedText.indices) {
            val textChar = simplifiedText[i].toInt() - MIN_CHAR_NUMBER
            val keyChar = generatedKey[i].toInt() - MIN_CHAR_NUMBER
            val resultChar = (textChar + keyChar) % NUMBER_OF_CHARACTERS
            encrypted.append((resultChar + MIN_CHAR_NUMBER).toChar())
        }
        return encrypted.toString()
    }

    override fun decrypt(text: String): String {
        val simplifiedText = simplifyText(text).toCharArray()
        val generatedKey = getKey(simplifiedText.size).toCharArray()
        val decrypted = StringBuilder()
        for (i in simplifiedText.indices) {
            val textChar = simplifiedText[i].toInt() - MIN_CHAR_NUMBER
            val keyChar = generatedKey[i].toInt() - MIN_CHAR_NUMBER
            val resultChar = (textChar - keyChar + NUMBER_OF_CHARACTERS) % NUMBER_OF_CHARACTERS
            decrypted.append((resultChar + MIN_CHAR_NUMBER).toChar())
        }
        return decrypted.toString()
    }

}