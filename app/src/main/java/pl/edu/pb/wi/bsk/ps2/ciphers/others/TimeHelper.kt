package pl.edu.pb.wi.bsk.ps2.ciphers.others

object TimeHelper {

    private const val MILLIS_IN_HOUR = 1000 * 60 * 60
    private const val MILLIS_IN_MINUTE = 1000 * 60
    private const val MILLIS_IN_SECOND = 1000

    fun millisToString(millis: Long): String {
        val hours: Long = millis / MILLIS_IN_HOUR
        var left = (millis - (hours * MILLIS_IN_HOUR))
        val minutes: Long = left / MILLIS_IN_MINUTE
        left -= minutes * MILLIS_IN_MINUTE
        val seconds: Long = left / MILLIS_IN_SECOND
        left -= seconds * MILLIS_IN_SECOND
        val milliseconds: Long = left
        val result = StringBuilder()
        if (hours > 0) result.append(hours).append(" godz. ")
        if (minutes > 0) result.append(minutes).append(" min. ")
        if (seconds > 0) result.append(seconds).append(" s ")
        result.append(milliseconds).append(" ms")
        return result.toString()
    }

}