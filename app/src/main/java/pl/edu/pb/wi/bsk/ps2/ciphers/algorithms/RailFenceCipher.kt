package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms

import pl.edu.pb.wi.bsk.ps2.ciphers.R
import pl.edu.pb.wi.bsk.ps2.ciphers.others.WrongKeyFormatException


class RailFenceCipher(key: String) : Algorithm(key) {

    private var railCount: Int

    init {
        try {
            railCount = key.toInt()
            if (railCount < 1) throw WrongKeyFormatException(R.string.should_be_numeric_format)
        } catch (e: Throwable) {
            throw WrongKeyFormatException(R.string.should_be_numeric_format)
        }
    }

    override fun encrypt(text: String): String {
        val charArray = Array(railCount) { CharArray(text.length) }
        var isGoingDown = false
        var rail = 0
        for (charIndex in text.indices) {
            if (rail == 0 || rail == railCount - 1) isGoingDown = !isGoingDown
            charArray[rail][charIndex] = text[charIndex]
            if (isGoingDown) rail++ else rail--
        }
        val encrypted = StringBuilder()
        for (r in 0 until railCount) {
            for (charIndex in text.indices) {
                if (charArray[r][charIndex].toInt() != 0) {
                    encrypted.append(charArray[r][charIndex])
                }
            }
        }
        return encrypted.toString()
    }

    override fun decrypt(text: String): String {
        val charArray = Array(railCount) { CharArray(text.length) }
        var isGoingDown = false
        var rail = 0
        for (charIndex in text.indices) {
            if (rail == 0 || rail == railCount - 1) isGoingDown = !isGoingDown
            charArray[rail][charIndex] = '\u9999'
            if (isGoingDown) rail++ else rail--
        }
        var index = 0
        for (r in 0 until railCount) {
            for (charIndex in text.indices) {
                if (charArray[r][charIndex] == '\u9999' && index < text.length) {
                    charArray[r][charIndex] = text[index++]
                }
            }
        }
        isGoingDown = false
        val decrypted = StringBuilder()
        rail = 0
        for (charIndex in text.indices) {
            if (rail == 0 || rail == railCount - 1) isGoingDown = !isGoingDown
            decrypted.append(charArray[rail][charIndex])
            if (isGoingDown) rail++ else rail--
        }
        return decrypted.toString()
    }

}