package pl.edu.pb.wi.bsk.ps2.ciphers.presentation

import android.content.ContentResolver
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pl.edu.pb.wi.bsk.ps2.ciphers.R
import pl.edu.pb.wi.bsk.ps2.ciphers.algorithms.Algorithm
import pl.edu.pb.wi.bsk.ps2.ciphers.algorithms.AlgorithmFactory
import pl.edu.pb.wi.bsk.ps2.ciphers.others.AlgorithmTypes
import pl.edu.pb.wi.bsk.ps2.ciphers.others.FileManager
import pl.edu.pb.wi.bsk.ps2.ciphers.others.NotImplementedException
import pl.edu.pb.wi.bsk.ps2.ciphers.others.WrongKeyFormatException

class MainViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var algorithm: Algorithm
    private val fileManager =
        FileManager()
    val output = MutableLiveData<Pair<String, Boolean>>()
    val input = MutableLiveData<String>()

    fun setAlgorithm(type: AlgorithmTypes, key: String): MutableLiveData<Int> {
        val result = MutableLiveData<Int>()
        try {
            algorithm = AlgorithmFactory.getAlgorithm(type, key)
            result.postValue(-1)
        } catch (e: WrongKeyFormatException) {
            result.postValue(e.messageRes)
        } catch (e: NotImplementedException) {
            result.postValue(R.string.not_implemented)
        }
        return result
    }

    fun encrypt(text: String) {
        Observable.fromCallable { algorithm.encrypt(text) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    output.postValue(Pair(it, true))
                },
                { output.postValue(Pair("", true)) }
            )
            .addToDisposables()
    }

    fun decrypt(text: String) {
        Observable.fromCallable { algorithm.decrypt(text) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    output.postValue(Pair(it, false))
                },
                { output.postValue(Pair("", false)) }
            )
            .addToDisposables()
    }

    fun writeToFile(text: String): MutableLiveData<Int> {
        val result = MutableLiveData<Int>()

        Observable.fromCallable { fileManager.writeToFile(text) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    result.postValue(if (it) R.string.write_to_file_success else R.string.write_to_file_error)
                },
                { result.postValue(R.string.write_to_file_error) }
            )
            .addToDisposables()

        return result
    }

    fun readFromFile(uri: Uri, contentResolver: ContentResolver): MutableLiveData<Int> {
        val result = MutableLiveData<Int>()

        Observable.fromCallable { fileManager.readFromFile(uri, contentResolver) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    if (it.isNullOrEmpty()) {
                        result.postValue(R.string.read_from_file_error)
                    } else {
                        input.postValue(it)
                        result.postValue(R.string.read_from_file_success)
                    }
                },
                { result.postValue(R.string.read_from_file_error) }
            )
            .addToDisposables()

        return result
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    private fun Disposable.addToDisposables() {
        compositeDisposable.add(this)
    }

}