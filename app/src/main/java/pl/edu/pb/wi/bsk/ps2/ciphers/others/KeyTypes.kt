package pl.edu.pb.wi.bsk.ps2.ciphers.others

enum class KeyTypes {
    NUMERIC,
    CHAR
}