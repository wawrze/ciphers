package pl.edu.pb.wi.bsk.ps2.ciphers.presentation

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import pl.edu.pb.wi.bsk.ps2.ciphers.R
import pl.edu.pb.wi.bsk.ps2.ciphers.others.AlgorithmTypes
import pl.edu.pb.wi.bsk.ps2.ciphers.others.KeyTypes
import pl.edu.pb.wi.bsk.ps2.ciphers.others.TimeHelper


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var selectedAlgorithm: AlgorithmTypes
    private var operationStart = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        setContentView(R.layout.activity_main)
        setupSpinner()
        setupButtons()
        viewModel.output.observe {
            if (it.first.isEmpty()) {
                Toast.makeText(this, R.string.unknown_error, Toast.LENGTH_LONG).show()
                return@observe
            }
            val operationEnd = System.currentTimeMillis()
            activity_main_output.setText(it.first)
            activity_main_progress_bar_container.visibility = View.GONE
            activity_main_last_operation.text = getString(
                R.string.last_operation,
                getString(
                    if (it.second) {
                        R.string.encrypt_operation
                    } else {
                        R.string.decrypt_operation
                    },
                    TimeHelper.millisToString(operationEnd - operationStart)
                )
            )
        }
        viewModel.input.observe {
            activity_main_input.setText(it)
        }
        checkPermission()
    }

    private fun setAlgorithm() {
        val key = activity_main_key_input.text.toString()
        viewModel.setAlgorithm(selectedAlgorithm, key).observe {
            if (it == -1) {
                applyCipher()
            } else {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun applyCipher() {
        activity_main_progress_bar_container.visibility = View.VISIBLE
        when {
            activity_main_encrypt_radio_button.isChecked -> {
                operationStart = System.currentTimeMillis()
                viewModel.encrypt(activity_main_input.text.toString())
            }
            activity_main_decrypt_radio_button.isChecked -> {
                operationStart = System.currentTimeMillis()
                viewModel.decrypt(activity_main_input.text.toString())
            }
        }
    }

    private fun setupButtons() {
        activity_main_confirm_button.setOnClickListener { setAlgorithm() }
        activity_main_rewrite_button.setOnClickListener {
            activity_main_input.text.clear()
            activity_main_input.text = activity_main_output.text
            activity_main_output.text.clear()
        }
        activity_main_input_from_file_button.setOnClickListener { chooseFileToRead() }
        activity_main_output_to_file_button.setOnClickListener {
            if (checkPermission()) {
                operationStart = System.currentTimeMillis()
                activity_main_progress_bar_container.visibility = View.VISIBLE
                viewModel.writeToFile(activity_main_output.text.toString()).observe {
                    activity_main_progress_bar_container.visibility = View.GONE
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    val operationLength =
                        TimeHelper.millisToString(System.currentTimeMillis() - operationStart)
                    activity_main_last_operation.text = getString(
                        R.string.last_operation,
                        getString(R.string.write_to_file_operation, operationLength)
                    )
                }
            } else {
                Toast.makeText(this, R.string.write_to_file_error, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setupSpinner() {
        val algorithms = AlgorithmTypes.values().map { it.algorithmName }.toList()
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, algorithms)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        activity_main_algorithm_spinner.adapter = adapter
        activity_main_algorithm_spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                selectedAlgorithm = AlgorithmTypes.getTypeForName(selectedItem)
                activity_main_key_input.inputType =
                    if (selectedAlgorithm.keyType == KeyTypes.CHAR) {
                        InputType.TYPE_CLASS_TEXT
                    } else {
                        InputType.TYPE_CLASS_NUMBER
                    }
                activity_main_key_input.text.clear()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == READ_FILE) {
            val uri = data?.data ?: return
            operationStart = System.currentTimeMillis()
            activity_main_progress_bar_container.visibility = View.VISIBLE
            viewModel.readFromFile(uri, contentResolver).observe {
                activity_main_progress_bar_container.visibility = View.GONE
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                val operationLength =
                    TimeHelper.millisToString(System.currentTimeMillis() - operationStart)
                activity_main_last_operation.text = getString(
                    R.string.last_operation,
                    getString(R.string.read_from_file_operation, operationLength)
                )
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun checkPermission() = if (
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED
        || ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED
    ) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PERMISSIONS_REQUEST
        )
        false
    } else {
        true
    }

    private fun chooseFileToRead() {
        if (!checkPermission()) return
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "*/*"
            putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("text/plain"))
            putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, READ_FILE)
        }
    }

    @MainThread
    private fun <T> MutableLiveData<T>.observe(action: (T) -> Unit) {
        this.observe(this@MainActivity, Observer { action.invoke(it) })
    }

    companion object {
        private const val PERMISSIONS_REQUEST = 555
        private const val READ_FILE = 666
    }

}
