package pl.edu.pb.wi.bsk.ps2.ciphers.algorithms;

import java.util.HashSet;

import pl.edu.pb.wi.bsk.ps2.ciphers.R;
import pl.edu.pb.wi.bsk.ps2.ciphers.others.KeyTransform;
import pl.edu.pb.wi.bsk.ps2.ciphers.others.WrongKeyFormatException;

public class TranspositionNumeric extends Algorithm {

    private int[] invertedPermutation;

    public TranspositionNumeric(String key) throws WrongKeyFormatException {
        super(key);
        char[] allowedChars = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-'};
        boolean allowed = false;
        try {
            for (int i = 0; i < key.length(); i++) {
                char ch = key.charAt(i);
                for (char allowedChar : allowedChars) {
                    if (allowedChar == ch) {
                        allowed = true;
                        break;
                    }
                }
                if (!allowed) {
                    throw new WrongKeyFormatException(R.string.should_have_specific_format);
                }
                allowed = false;
            }
            invertedPermutation = KeyTransform.numericKeyTransform(key);

            HashSet<Integer> keyElementsSet = new HashSet<>();
            for (int element : invertedPermutation) {
                keyElementsSet.add(element);
            }
            if (keyElementsSet.size() < invertedPermutation.length) {
                throw new WrongKeyFormatException(R.string.should_contain_unique_number);
            }
            for (int i = 0; i < invertedPermutation.length; i++) {
                if (!keyElementsSet.contains(i + 1)) {
                    throw new WrongKeyFormatException(R.string.should_contain_every_number);
                }
            }
        } catch (WrongKeyFormatException e) {
            throw e;
        } catch (Exception e) {
            throw new WrongKeyFormatException(R.string.should_have_specific_format);
        }
    }

    @Override
    public String decrypt(String text) {
        int[] permutationOrder = invertedPermutation.clone();
        int col = permutationOrder.length;
        int row = rowsAmount(text, col);

        char[][] matrix = new char[row][col];
        StringBuilder decryptedString = new StringBuilder();
        for (int i = 0; i < permutationOrder.length; i++) {
            invertedPermutation[permutationOrder[i] - 1] = i + 1;
        }
        int arraySize = row * col;
        int charactersLeft = arraySize - text.length();
        for (int c = 0; c < charactersLeft; c++) {
            matrix[row - 1][col - 1 - c] = 251;
        }
        for (int j = 0, k = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                if (k < text.length()) {
                    if (matrix[i][invertedPermutation[j] - 1] != 251) {
                        matrix[i][invertedPermutation[j] - 1] = text.charAt(k);
                        k++;
                    }
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (matrix[i][j] != 251) decryptedString.append(matrix[i][j]);
            }
        }
        return decryptedString.toString();
    }

    @Override
    public String encrypt(String text) {
        int[] permutationOrder = invertedPermutation.clone();
        int col = permutationOrder.length;
        int row = rowsAmount(text, col);
        StringBuilder encryptedString = new StringBuilder();

        if (text.length() % col >= 1) row++;

        char[][] matrix = new char[row][col];
        char[][] encrypted = new char[row][col];

        for (int i = 0, k = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (k < text.length()) {
                    matrix[i][j] = text.charAt(k);
                    k++;
                } else {
                    matrix[i][j] = 251;
                }
            }
        }
        for (int j = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                encrypted[i][permutationOrder[j] - 1] = matrix[i][j];
            }
        }
        for (int j = 0; j < col; j++) {
            for (int i = 0; i < row; i++) {
                if (encrypted[i][j] != 251) encryptedString.append(encrypted[i][j]);
            }
        }
        return encryptedString.toString();
    }

    private int rowsAmount(String text, int col) {
        if ((text.length() % col) == 0) {
            return text.length() / col;
        } else {
            return text.length() / col + 1;
        }
    }

}